<?php

/**
 * @file
 * Install and update hooks for Panopoly Demo.
 */

use Drupal\views\Entity\View;

/**
 * Implements hook_install().
 */
function panopoly_demo_install() {
  $migration_ids = [
    'panopoly_demo_node',
    'panopoly_demo_menu',
  ];
  /** @var \Drupal\panopoly_core\MigrateHelper $migrate_helper */
  $migrate_helper = \Drupal::service('panopoly_core.migrate_helper');
  $migrate_helper->import($migration_ids);

  // Point the front page to our landing page demo.
  _panopoly_demo_set_front_page('/demo');

  // Install database search by default.
  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
  $module_installer = \Drupal::service('module_installer');
  $module_installer->install(['panopoly_search_db']);
}

/**
 * Implements hook_uninstall().
 */
function panopoly_demo_uninstall() {
  $migration_ids = [
    'panopoly_demo_node',
    'panopoly_demo_menu',
  ];
  /** @var \Drupal\panopoly_core\MigrateHelper $migrate_helper */
  $migrate_helper = \Drupal::service('panopoly_core.migrate_helper');
  $migrate_helper->rollback($migration_ids);

  // Reset the homepage.
  _panopoly_demo_set_front_page('/node');
}

/**
 * Helper to set the front page.
 *
 * @param string $path
 *   The path for the front page.
 */
function _panopoly_demo_set_front_page($path) {
  $site_config = \Drupal::configFactory()->getEditable('system.site');
  $site_page_settings = $site_config->get('page');

  // Only set the homepage if the site does not doesn't already have custom
  // front page.
  if (in_array($site_page_settings['front'], ['', '/user/login', '/node'])) {
    $site_page_settings['front'] = $path;
    $site_config->set('page', $site_page_settings);
    $site_config->save();
  }
}

/**
 * Implements hook_update_dependencies().
 */
function panopoly_demo_update_dependencies() {
  $dependencies = [];
  $dependencies['panopoly_demo'][8001] = [
    'panopoly' => 8001,
  ];
  return $dependencies;
}

/**
 * Swap out core search form for Panopoly Search.
 */
function panopoly_demo_update_8001() {
  if (!\Drupal::moduleHandler()->moduleExists('panopoly_search_db')) {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
    $module_installer = \Drupal::service("module_installer");
    $module_installer->install(['panopoly_search_db']);
  }

  $config = \Drupal::configFactory()->getEditable('page_manager.page_variant.panopoly_demo_home-layout_builder');
  $data = $config->getRawData();
  if (!empty($data['variant_settings']['sections'][0]['components']['f3683a73-c6a6-4148-8ddf-eb14fe74aa7c'])) {
    unset($data['variant_settings']['sections'][0]['components']['f3683a73-c6a6-4148-8ddf-eb14fe74aa7c']);
    $data['variant_settings']['sections'][0]['components']['98712ed7-96d2-4c77-886a-0c8b035d6704'] = [
      'uuid' => '98712ed7-96d2-4c77-886a-0c8b035d6704',
      'region' => 'sidebar',
      'configuration' => [
        'id' => 'panopoly_search_box',
        'label' => 'Search box',
        'provider' => 'panopoly_search',
        'label_display' => '0',
        'context_mapping' => [],
      ],
      'additional' => [],
      'weight' => -1,
    ];
    if (!isset($data['variant_settings']['title_type'])) {
      $data['variant_settings']['title_type'] = 'label';
      $data['variant_settings']['manual_title'] = '';
      $data['variant_settings']['title_from_block'] = '';
    }
    $config->setData($data);
    $config->save(TRUE);
  }
}

/**
 * Add exposed_form and display_type to Widgets view.
 */
function panopoly_demo_update_8002() {
  if (\Drupal::moduleHandler()->moduleExists('views')) {
    $view = View::load('panopoly_demo');
    if (!$view) {
      return;
    }
    $default_display = &$view->getDisplay('default');
    $default_display['display_options']['fields']['name']['admin_label'] = 'Posted by';
    $default_display['display_options']['sorts']['created']['exposed'] = TRUE;
    $default_display['display_options']['sorts']['created']['expose'] = [
      'label' => 'Post date',
    ];
    $default_display['display_options']['sorts']['changed'] = [
      'id' => 'changed',
      'table' => 'node_field_data',
      'field' => 'changed',
      'relationship' => 'none',
      'group_type' => 'group',
      'admin_label' => '',
      'order' => 'DESC',
      'exposed' => TRUE,
      'expose' => [
        'label' => 'Updated date',
      ],
      'granularity' => 'second',
      'entity_type' => 'node',
      'entity_field' => 'changed',
      'plugin_id' => 'date',
    ];
    $default_display['display_options']['sorts']['title'] = [
      'id' => 'title',
      'table' => 'node_field_data',
      'field' => 'title',
      'relationship' => 'none',
      'group_type' => 'group',
      'admin_label' => '',
      'order' => 'ASC',
      'exposed' => TRUE,
      'expose' => [
        'label' => 'Title',
      ],
      'entity_type' => 'node',
      'entity_field' => 'title',
      'plugin_id' => 'standard',
    ];
    $default_display['cache_metadata']['contexts'] = [
      'languages:language_content',
      'languages:language_interface',
      'url',
      'url.query_args:sort_by',
      'url.query_args:sort_order',
      'user.node_grants:view',
      'user.permissions',
    ];
    $demo_listing_display = &$view->getDisplay('demo_listing');
    $demo_listing_display['display_options']['allow']['display_type'] = 'display_type';
    $demo_listing_display['display_options']['allow']['hide_fields'] = 'hide_fields';
    $demo_listing_display['display_options']['allow']['sort_fields'] = 'sort_fields';
    $demo_listing_display['display_options']['allow']['exposed_form'] = 'exposed_form';
    $demo_listing_display['display_options']['allow']['configure_sorts'] = '0';
    $demo_listing_display['display_options']['allow']['disable_filters'] = '0';
    $demo_listing_display['display_options']['allow']['offset'] = '0';
    $demo_listing_display['display_options']['allow']['pager'] = '0';
    $demo_listing_display['cache_metadata']['contexts'] = [
      'languages:language_content',
      'languages:language_interface',
      'url',
      'url.query_args:sort_by',
      'url.query_args:sort_order',
      'user.node_grants:view',
      'user.permissions',
    ];
    $list_featured_demo_items_display = &$view->getDisplay('list_featured_demo_items');
    $list_featured_demo_items_display['display_options']['allow']['display_type'] = 'display_type';
    $list_featured_demo_items_display['display_options']['allow']['hide_fields'] = 'hide_fields';
    $list_featured_demo_items_display['display_options']['allow']['sort_fields'] = 'sort_fields';
    $list_featured_demo_items_display['display_options']['allow']['exposed_form'] = 'exposed_form';
    $list_featured_demo_items_display['display_options']['allow']['configure_sorts'] = '0';
    $list_featured_demo_items_display['display_options']['allow']['disable_filters'] = '0';
    $list_featured_demo_items_display['display_options']['allow']['offset'] = '0';
    $list_featured_demo_items_display['display_options']['allow']['pager'] = '0';
    $list_featured_demo_items_display['display_options']['fields']['name']['admin_label'] = 'Posted by';
    $list_featured_demo_items_display['cache_metadata']['contexts'] = [
      'languages:language_content',
      'languages:language_interface',
      'url',
      'url.query_args:sort_by',
      'url.query_args:sort_order',
      'user.node_grants:view',
      'user.permissions',
    ];
    $view->save();
  }
}
