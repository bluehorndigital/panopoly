<?php

namespace Drupal\panopoly_magic\Plugin\views\display;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ctools_views\Plugin\Display\Block;
use Drupal\views\Plugin\Block\ViewsBlock;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides display type overrides for the Block display.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName.LowerCamelName
 */
final class MagicBlock extends Block {

  /**
   * The default display.
   *
   * This gets magically set *somewhere* and is used to derive the settings
   * for the style and row plugins in this display plugin.
   *
   * @var \Drupal\views\Plugin\views\display\DefaultDisplay
   */
  public $default_display;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.block'),
      $container->get('plugin.manager.views.filter'),
      $container->get('request_stack')->getCurrentRequest()
    );
    $instance->setEntityDisplayRepository($container->get('entity_display.repository'));
    return $instance;
  }

  /**
   * Set the entity display repository.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function setEntityDisplayRepository(EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);
    $filtered_allow = array_filter($this->getOption('allow'));
    if (isset($filtered_allow['display_type'])) {
      if ($options['allow']['value'] === $this->t('None')) {
        $options['allow']['value'] = $this->t('Display type');
      }
      else {
        $options['allow']['value'] .= ', ' . $this->t('Display type');
      }
    }
    if (isset($filtered_allow['exposed_form'])) {
      if ($options['allow']['value'] === $this->t('None')) {
        $options['allow']['value'] = $this->t('Use exposed form as block configuration');
      }
      else {
        $options['allow']['value'] .= ', ' . $this->t('Use exposed form as block configuration');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['allow']['#options']['display_type'] = $this->t('Display type');
    $form['allow']['#options']['exposed_form'] = $this->t('Use exposed widgets form as block configuration');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(ViewsBlock $block, array &$form, FormStateInterface $form_state) {
    $form = parent::blockForm($block, $form, $form_state);

    $form['override']['#type'] = 'container';
    $form['override']['#weight'] = 50;

    $allow_settings = array_filter($this->getOption('allow'));

    // @todo this prevents `exposed_form` from being added.
    // rework the code so its somewhat manageable with logic check.
    if (empty($allow_settings['display_type'])) {
      return $form;
    }

    $block_configuration = $block->getConfiguration();

    $current_row_plugin = $this->getOption('row')['type'];
    // Set to default view settings if there isn't one.
    if (empty($block_configuration['view_settings'])) {
      // Normalize the "entity" row plugin derivatives.
      $block_configuration['view_settings'] = $this->convertViewSettings($current_row_plugin);
    }
    $block_configuration['view_settings'] = $this->convertViewSettings($block_configuration['view_settings']);

    // Add information about the View Mode.
    $form['override']['display_settings']['view_settings'] = [
      '#type' => 'radios',
      '#prefix' => '<div class="view-settings-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Display Type'),
      '#default_value' => $block_configuration['view_settings'],
      '#weight' => 10,
      '#options' => [
        'fields' => $this->t('Fields'),
        'rendered_entity' => $this->t('Content'),
        'table' => $this->t('Table'),
      ],
    ];

    // Add header column options for table views.
    $form['override']['display_settings']['header_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Column Header'),
      '#options' => [
        'none' => $this->t('None'),
        'titles' => $this->t('Titles'),
      ],
      '#default_value' => !empty($block_configuration['header_type']) ? $block_configuration['header_type'] : 'none',
      '#states' => [
        'visible' => [
          ':input[name="settings[override][display_settings][view_settings]"]' => ['value' => 'table'],
        ],
      ],
      '#weight' => 11,
    ];

    // Update field overrides to be dependent on the view settings selection.
    if (!empty($form['override']['order_fields'])) {
      $form['override']['order_fields']['#weight'] = 15;
      // @note states target js-form-wrapper, which tables do not have by default.
      $form['override']['order_fields']['#attributes']['class'][] = 'js-form-wrapper';
      $form['override']['order_fields']['#attributes']['class'][] = 'form-wrapper';
      $form['override']['order_fields']['#states'] = [
        // The inverted logic here isn't optimal, and in the future may be
        // better achieved via OR'd conditions.
        // @link http://drupal.org/node/735528 @endlink
        'invisible' => [
          ':input[name="settings[override][display_settings][view_settings]"]' => ['value' => 'rendered_entity'],
        ],
      ];
    }

    // Get view modes for entity.
    $view_modes = $this->viewModeOptions($this->view->getBaseEntityType()->id());
    $options_default_view_mode = ($current_row_plugin === 'fields') ? 'teaser' : 'full';
    $row_options = $this->getOption('row');
    if (!empty($row_options['view_mode'])) {
      $options_default_view_mode = $this->getOption('row')['view_mode'];
    }
    if (!array_key_exists($options_default_view_mode, $view_modes)) {
      $options_default_view_mode = key($view_modes);
    }
    // Add specific style options.
    $form['override']['display_settings']['content_settings'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="settings[override][display_settings][view_settings]"]' => ['value' => 'rendered_entity'],
        ],
      ],
      '#weight' => 15,
    ];
    // @todo finish porting visibility on lines 1177-1192.
    $form['override']['display_settings']['content_settings']['view_mode'] = [
      '#type' => 'radios',
      '#options' => $view_modes,
      '#default_value' => !empty($block_configuration['content_settings']['view_mode']) ? $block_configuration['content_settings']['view_mode'] : $options_default_view_mode,
    ];

    // Modify the way ctools exposed sorts to meet 1.x functionality.
    if (!empty($allow_settings['exposed_form'])) {
      $form['override']['exposed_form'] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];
      $filters = array_filter($this->getHandlers('filter'), static function (ViewsHandlerInterface $plugin) {
        return $plugin->isExposed();
      });
      $form['override']['exposed_form']['filters'] = [
        '#type' => 'container',
        '#tree' => TRUE,
        '#access' => count($filters) > 0,
      ];
      // Let form plugins know this is for exposed widgets.
      $form_state->set('exposed', TRUE);
      foreach ($filters as $filter_name => $filter) {
        assert($filter instanceof FilterPluginBase);
        $filter->buildExposedForm($form['override']['exposed_form']['filters'], $form_state);
        $form['override']['exposed_form']['filters'][$filter_name]['#title'] = $filter->adminLabel();
      }
      $form_state->set('exposed', NULL);

      $sorts = array_filter($this->getHandlers('sort'), static function (ViewsHandlerInterface $plugin) {
        return $plugin->isExposed();
      });
      $form['override']['exposed_form']['sort'] = [
        '#type' => 'container',
        'sort_order' => [
          '#title' => $this->t('Sort order'),
          '#type' => 'radios',
          '#options' => [
            'ASC' => $this->t('Sort ascending'),
            'DESC' => $this->t('Sort descending'),
          ],
          '#default_value' => $block_configuration['sort_order'] ?? 'ASC',
        ],
        'sort_by' => [
          '#title' => $this->t('Sort by'),
          '#type' => 'select',
          '#options' => array_map(static function (ViewsHandlerInterface $plugin) {
            // @todo exposed info label instead
            return $plugin->adminLabel();
          }, $sorts),
          '#default_value' => $block_configuration['sort_by'] ?? '',
        ],
        '#access' => count($sorts) > 0,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit(ViewsBlock $block, $form, FormStateInterface $form_state) {
    parent::blockSubmit($block, $form, $form_state);
    $configuration = $block->getConfiguration();
    $allow_settings = array_filter($this->getOption('allow'));

    if (!empty($allow_settings['display_type'])) {
      $display_settings = $form_state->getValue(['override', 'display_settings']);
      foreach ($display_settings as $setting => $value) {
        $configuration[$setting] = $value;
      }
    }
    if (!empty($allow_settings['exposed_form'])) {
      $sort = $form_state->getValue(['override', 'exposed_form', 'sort']);
      foreach ($sort as $setting => $value) {
        $configuration['exposed_form']['sort'][$setting] = $value;
      }
      $sort = $form_state->getValue(['override', 'exposed_form', 'filters']);
      foreach ($sort as $setting => $value) {
        $configuration['exposed_form']['filters'][$setting] = $value;
      }
    }
    $block->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function preBlockBuild(ViewsBlock $block) {
    $config = $block->getConfiguration();
    [, $display_id] = explode('-', $block->getDerivativeId(), 2);

    // @see panopoly_magic_views_pre_view().
    $allow_settings = array_filter($this->getOption('allow'));
    if (!empty($allow_settings['display_type']) && !empty($config['view_settings'])) {
      $view_settings = $this->convertViewSettings($config['view_settings']);

      // Set the style plugin to a table style.
      // Determine that this was previously a field view, which has been
      // overridden to a node view in the pane config.
      if ($view_settings === 'rendered_entity') {
        $this->options['defaults']['row'] = FALSE;
        $this->options['row']['type'] = 'entity:' . $this->view->getBaseEntityType()->id();
        if (!empty($config['content_settings']['view_mode'])) {
          // Transfer over the row options from default if set to use.
          if (!empty($this->options['defaults']['row_options'])) {
            $this->options['defaults']['row_options'] = FALSE;
          }
          $this->options['row']['options']['view_mode'] = $config['content_settings']['view_mode'];
        }
      }
      elseif ($config['view_settings'] === 'fields') {
        $this->options['defaults']['row'] = FALSE;
        $this->options['row']['type'] = 'fields';
      }
      elseif ($config['view_settings'] === 'table') {
        // Find the currently active field definition, else break out as table
        // needs fields.
        if (!empty($this->options['fields'])) {
          $fields = &$this->options['fields'];
        }
        elseif (empty($this->default_display->options['defaults']['fields']) && isset($this->view->display_handler->options['fields'])) {
          $fields = &$this->default_display->options['fields'];
        }
        else {
          // If no fields, don't try to display as table.
          return;
        }

        $this->options['defaults']['style'] = FALSE;
        $this->options['style']['type'] = 'table';

        // Set or remove header labels depending on user selection.
        $use_header_titles = !empty($config['header_type']) && $config['header_type'] === 'titles';
        foreach ($fields as $field_key => &$field) {
          if ($use_header_titles && !empty($field['admin_label']) && empty($field['label'])) {
            $field['label'] = $field['admin_label'];
          }
          elseif (!$use_header_titles) {
            $field['label'] = '';
          }
          // Hide empty columns.
          if (!empty($this->options['row']['hide_empty'])) {
            $this->options['style'][$field_key]['empty_column'] = TRUE;
          }
        }
        unset($field);
      }
    }
    // The ctools Block plugin invokes the style plugin and instantiates it
    // first, so we run it after we've adjusted row and style options.
    parent::preBlockBuild($block);

    if (!empty($allow_settings['exposed_form'])) {
      if (!empty($config['exposed_form']['sort'])) {
        $sort_order = $config['exposed_form']['sort']['sort_order'];
        $sort_by = $config['exposed_form']['sort']['sort_by'];
        $sorts = $this->view->getHandlers('sort', $display_id);
        foreach ($sorts as $sort_name => $sort) {
          if (!$sort['exposed']) {
            continue;
          }
          if ($sort_name !== $sort_by) {
            $this->view->removeHandler($display_id, 'sort', $sort_name);
          }
          else {
            $sort['order'] = $sort_order;
            $sort['exposed'] = FALSE;
            $this->view->setHandler($display_id, 'sort', $sort_name, $sort);
          }
        }
      }
      $filters = $config['exposed_form']['filters'] ?? [];
      foreach ($filters as $filter_name => $value) {
        $filter = $this->view->getHandler($display_id, 'filter', $filter_name);
        $filter['exposed'] = FALSE;
        if ($value !== 'All' && $value !== []) {
          $filter['value'] = !is_array($value) ? [$value] : $value;
        }
        $this->view->setHandler($display_id, 'filter', $filter_name, $filter);
      }
    }

  }

  /**
   * Convert with legacy 'nodes' and others (such as 'files') view settings.
   *
   * @todo copy and paste; used to help handle `entity:` derivatives.
   *
   * @param string $view_setting
   *   The view setting value.
   *
   * @return string
   *   The converted setting value.
   */
  private function convertViewSettings(string $view_setting): string {
    // The 'fields' and 'table' view settings apply to any entity type.
    if (in_array($view_setting, ['fields', 'table'])) {
      return $view_setting;
    }

    // We convert other view settings to 'rendered_entity' (which could be
    // 'entity:node' or 'entity:file' and others specific to an entity type).
    return 'rendered_entity';
  }

  /**
   * Get view mode options for an entity type.
   *
   * @param string $id
   *   The entity type ID.
   *
   * @return array
   *   The view mode options.
   */
  private function viewModeOptions(string $id): array {
    $options = $this->entityDisplayRepository->getViewModeOptions($id);
    // When selecting full, locally the site becomes unavailable with a 502
    // gateway timeout. Default and full should be the same.
    unset($options['full']);
    return $options;
  }

}
