Feature: Panopoly Core Dependencies (#2183937)
  In order to build a child distribution
  As a developer
  I shouldn't experience installation errors

  @panopoly2
  Scenario:
    Given "panopoly_core" has the "drupal:taxonomy" dependency at position "7"
