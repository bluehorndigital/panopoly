<?php

namespace Drupal\panopoly_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Panopoly Core configuration form.
 */
class PanopolyCoreSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'panopoly_core_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['panopoly_core.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('panopoly_core.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];
    $form['settings']['layout_builder_tray_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Layout Builder tray width'),
      '#default_value' => $config->get('layout_builder_tray_width'),
      '#description' => $this->t(
        'Width in pixels with no units (e.g. "<code>768</code>"). See <a href=":link">the jQuery Dialog documentation</a> for more details.',
        [':link' => 'https://api.jqueryui.com/dialog/#option-width']
      ),
      '#min' => 1,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('panopoly_core.settings')
      ->set('layout_builder_tray_width', $form_state->getValue('layout_builder_tray_width'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
